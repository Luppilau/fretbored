export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: "FretBored",
    title: "FretBored",
    script: [
      { src: 'https://js.stripe.com/v3/', },
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ,
      {
        rel: 'stylesheet',
          href: [
            'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap',
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
        ]
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
      '~/assets/style/app.scss',
      '~/assets/variables.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/moment.js'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    "@nuxtjs/vuetify",
    '@nuxtjs/pwa'
  ],


  vuetify: {
    customVariables: ["~/assets/variables.scss"],
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */

    extend(config, ctx) {

    }
  }
}
